#include <time.h>
#include <stdlib.h>

//musi byt sude cislo
#define MAX_CLIENTS 20

#define TRUE 1
#define FALSE 0
#define NUMBER_OF_SHAPES 7
#define NUMBER_OF_BLOCKS 4
#define WIDTH 10
#define HEIGHT 20
#define DEFAULT_X 4
#define DEFAULT_Y 0

int current_game;
int currentPlayer;

int isGamePaused[MAX_CLIENTS / 2], gameEnded[MAX_CLIENTS / 2];
struct timespec timeNow;

int shape1[MAX_CLIENTS / 2], shape2[MAX_CLIENTS / 2];
int bag1[MAX_CLIENTS / 2][NUMBER_OF_SHAPES], bag2[MAX_CLIENTS / 2][NUMBER_OF_SHAPES];
int xBlocks1[MAX_CLIENTS / 2][NUMBER_OF_BLOCKS], xBlocks2[MAX_CLIENTS / 2][NUMBER_OF_BLOCKS];
int yBlocks1[MAX_CLIENTS / 2][NUMBER_OF_BLOCKS], yBlocks2[MAX_CLIENTS / 2][NUMBER_OF_BLOCKS];
int grid1[MAX_CLIENTS / 2][WIDTH][HEIGHT], grid2[MAX_CLIENTS / 2][WIDTH][HEIGHT];
int yHint1[MAX_CLIENTS / 2][NUMBER_OF_BLOCKS], yHint2[MAX_CLIENTS / 2][NUMBER_OF_BLOCKS];
int rotation1[MAX_CLIENTS / 2], xPos1[MAX_CLIENTS / 2], yPos1[MAX_CLIENTS / 2], lastShiftX1[MAX_CLIENTS / 2], lastShiftY1[MAX_CLIENTS / 2], freeSlotsInBag1[MAX_CLIENTS / 2];
int rotation2[MAX_CLIENTS / 2], xPos2[MAX_CLIENTS / 2], yPos2[MAX_CLIENTS / 2], lastShiftX2[MAX_CLIENTS / 2], lastShiftY2[MAX_CLIENTS / 2], freeSlotsInBag2[MAX_CLIENTS / 2];
int playerRestart1[MAX_CLIENTS / 2], playerPause1[MAX_CLIENTS / 2], playerUnpause1[MAX_CLIENTS / 2], playerQ1[MAX_CLIENTS / 2];
int playerE1[MAX_CLIENTS / 2], playerA1[MAX_CLIENTS / 2], playerS1[MAX_CLIENTS / 2], playerD1[MAX_CLIENTS / 2], playerSpace1[MAX_CLIENTS / 2];
int playerRestart2[MAX_CLIENTS / 2], playerPause2[MAX_CLIENTS / 2], playerUnpause2[MAX_CLIENTS / 2], playerQ2[MAX_CLIENTS / 2], playerE2[MAX_CLIENTS / 2];
int playerA2[MAX_CLIENTS / 2], playerS2[MAX_CLIENTS / 2], playerD2[MAX_CLIENTS / 2], playerSpace2[MAX_CLIENTS / 2];
int removedRows1[MAX_CLIENTS / 2], removedRows2[MAX_CLIENTS / 2];
uint64_t timeSinceDown1[MAX_CLIENTS / 2], previousGameTime1[MAX_CLIENTS / 2];
uint64_t timeSinceDown2[MAX_CLIENTS / 2], previousGameTime2[MAX_CLIENTS / 2];

void shapeI(int rotation, int xBlocks[], int yBlocks[], int x, int y)
{
	switch(rotation)
	{
		case 0:
			xBlocks[0] = x;
			xBlocks[1] = x + 1;
			xBlocks[2] = x + 2;
			xBlocks[3] = x + 3;
			yBlocks[0] = yBlocks[1] = yBlocks[2] = yBlocks[3] = y + 1;
			break;
		case 1:
			yBlocks[0] = y;
			yBlocks[1] = y + 1;
			yBlocks[2] = y + 2;
			yBlocks[3] = y + 3;
			xBlocks[0] = xBlocks[1] = xBlocks[2] = xBlocks[3] = x + 2;
			break;
		case 2:
			xBlocks[0] = x;
			xBlocks[1] = x + 1;
			xBlocks[2] = x + 2;
			xBlocks[3] = x + 3;
			yBlocks[0] = yBlocks[1] = yBlocks[2] = yBlocks[3] = y + 2;
			break;
		case 3:
			yBlocks[0] = y;
			yBlocks[1] = y + 1;
			yBlocks[2] = y + 2;
			yBlocks[3] = y + 3;
			xBlocks[0] = xBlocks[1] = xBlocks[2] = xBlocks[3] = x + 1;
			break;
		default:
			printf("error");
	}
}

void shapeO(int rotation, int xBlocks[], int yBlocks[], int x, int y)
{
	xBlocks[0] = xBlocks[2] = x;
	xBlocks[1] = xBlocks[3] = x + 1;
	yBlocks[0] = yBlocks[1] = y;
	yBlocks[2] = yBlocks[3] = y + 1;
}

void shapeT(int rotation, int xBlocks[], int yBlocks[], int x, int y)
{
	switch(rotation)
	{
		case 0:
			xBlocks[0] = xBlocks[2] = x + 1;
			xBlocks[1] = x;
			xBlocks[3] = x + 2;
			yBlocks[0] = y;
			yBlocks[1] = yBlocks[2] = yBlocks[3] = y + 1;
			break;
		case 1:
			xBlocks[0] = xBlocks[2] = x + 1;
			xBlocks[1] = x + 1;
			xBlocks[3] = x + 2;
			yBlocks[0] = y;
			yBlocks[1] = y + 2;
			yBlocks[2] = yBlocks[3] = y + 1;
			break;
		case 2:
			xBlocks[0] = xBlocks[2] = x + 1;
			xBlocks[1] = x;
			xBlocks[3] = x + 2;
			yBlocks[0] = y + 2;
			yBlocks[1] = yBlocks[2] = yBlocks[3] = y + 1;
			break;
		case 3:
			xBlocks[0] = xBlocks[2] = x + 1;
			xBlocks[1] = x + 1;
			xBlocks[3] = x;
			yBlocks[0] = y;
			yBlocks[1] = y + 2;
			yBlocks[2] = yBlocks[3] = y + 1;
			break;
		default:
			printf("error");
	}
}

void shapeS(int rotation, int xBlocks[], int yBlocks[], int x, int y)
{
	switch(rotation)
	{
		case 0:
			xBlocks[0] = x;
			xBlocks[1] = xBlocks[2] = x + 1;
			xBlocks[3] = x + 2;
			yBlocks[0] = yBlocks[1] = y + 1;
			yBlocks[2] = yBlocks[3] = y;
			break;
		case 1:
			xBlocks[0] = xBlocks[1] = x + 1;
			xBlocks[2] = xBlocks[3] = x + 2;
			yBlocks[0] = y;
			yBlocks[1] = yBlocks[2] = y + 1;
			yBlocks[3] = y + 2;
			break;
		case 2:
			xBlocks[0] = x;
			xBlocks[1] = xBlocks[2] = x + 1;
			xBlocks[3] = x + 2;
			yBlocks[0] = yBlocks[1] = y + 2;
			yBlocks[2] = yBlocks[3] = y + 1;
			break;
		case 3:
			xBlocks[0] = xBlocks[1] = x;
			xBlocks[2] = xBlocks[3] = x + 1;
			yBlocks[0] = y;
			yBlocks[1] = yBlocks[2] = y + 1;
			yBlocks[3] = y + 2;
			break;
		default:
			printf("error");
	}
}

void shapeZ(int rotation, int xBlocks[], int yBlocks[], int x, int y)
{
	switch(rotation)
	{
		case 0:
			xBlocks[0] = x + 2;
			xBlocks[1] = xBlocks[2] = x + 1;
			xBlocks[3] = x;
			yBlocks[0] = yBlocks[1] = y + 1;
			yBlocks[2] = yBlocks[3] = y;
			break;
		case 1:
			xBlocks[0] = xBlocks[1] = x + 1;
			xBlocks[2] = xBlocks[3] = x + 2;
			yBlocks[0] = y + 2;
			yBlocks[1] = yBlocks[2] = y + 1;
			yBlocks[3] = y;
			break;
		case 2:
			xBlocks[0] = x + 2;
			xBlocks[1] = xBlocks[2] = x + 1;
			xBlocks[3] = x;
			yBlocks[0] = yBlocks[1] = y + 2;
			yBlocks[2] = yBlocks[3] = y + 1;
			break;
		case 3:
			xBlocks[0] = xBlocks[1] = x;
			xBlocks[2] = xBlocks[3] = x + 1;
			yBlocks[0] = y + 2;
			yBlocks[1] = yBlocks[2] = y + 1;
			yBlocks[3] = y;
			break;
		default:
			printf("error");
	}
}

void shapeL(int rotation, int xBlocks[], int yBlocks[], int x, int y)
{
	switch(rotation)
	{
		case 0:
			xBlocks[0] = x;
			xBlocks[1] = x + 1;
			xBlocks[2] = xBlocks[3] = x + 2;
			yBlocks[0] = yBlocks[1] = yBlocks[2] = y + 1;
			yBlocks[3] = y;
			break;
		case 1:
			xBlocks[0] = xBlocks[1] = xBlocks[2] = x + 1;
			xBlocks[3] = x + 2;
			yBlocks[0] = y;
			yBlocks[1] = y + 1;
			yBlocks[2] = yBlocks[3] = y + 2;
			break;
		case 2:
			xBlocks[0] = xBlocks[3] = x;
			xBlocks[1] = x + 1;
			xBlocks[2] = x + 2;
			yBlocks[0] = yBlocks[1] = yBlocks[2] = y + 1;
			yBlocks[3] = y + 2;
			break;
		case 3:
			xBlocks[0] = xBlocks[1] = xBlocks[2] = x + 1;
			xBlocks[3] = x;
			yBlocks[0] = yBlocks[3] = y;
			yBlocks[1] = y + 1;
			yBlocks[2] = y + 2;
			break;
		default:
			printf("error");
	}
}

void shapeJ(int rotation, int xBlocks[], int yBlocks[], int x, int y)
{
	switch(rotation)
	{
		case 0:
			xBlocks[0] = xBlocks[3] = x;
			xBlocks[1] = x + 1;
			xBlocks[2] = x + 2;
			yBlocks[0] = yBlocks[1] = yBlocks[2] = y + 1;
			yBlocks[3] = y;
			break;
		case 1:
			xBlocks[0] = xBlocks[1] = xBlocks[2] = x + 1;
			xBlocks[3] = x + 2;
			yBlocks[0] = yBlocks[3] = y;
			yBlocks[1] = y + 1;
			yBlocks[2] = y + 2;
			break;
		case 2:
			xBlocks[0] = x;
			xBlocks[1] = x + 1;
			xBlocks[2] = xBlocks[3] = x + 2;
			yBlocks[0] = yBlocks[1] = yBlocks[2] = y + 1;
			yBlocks[3] = y + 2;
			break;
		case 3:
			xBlocks[0] = xBlocks[1] = xBlocks[2] = x + 1;
			xBlocks[3] = x;
			yBlocks[0] = y;
			yBlocks[1] = y + 1;
			yBlocks[2] = yBlocks[3] = y + 2;
			break;
		default:
			printf("error");
	}
}

void (*changeShape[NUMBER_OF_SHAPES])(int rotation, int xBlocks[], int yBlocks[], int x, int y) = {shapeI, shapeO, shapeT, shapeS, shapeZ, shapeJ, shapeL};

void updateShape(int shape, int rotation, int xBlocks[], int yBlocks[], int x, int y)
{
	(*changeShape[shape])(rotation, xBlocks, yBlocks, x, y);
}

int isDuplicate(const int bag[], int index)
{
	for(int i = 0; i < index; ++i)
	{
		if(bag[index] == bag[i])
		{
			return TRUE;
		}
	}
	return FALSE;
}

void fillBag(int bag[], int *freeSlotsInBag)
{
	for(int i = 0; i < NUMBER_OF_SHAPES; ++i)
	{
		bag[i] = rand() % NUMBER_OF_SHAPES;
		while(isDuplicate(bag, i))
		{
			if(++bag[i] == NUMBER_OF_SHAPES)
			{
				bag[i] = 0;
			}
		}
	}
	*freeSlotsInBag = 0;
}

void right(int xBlocks[], int *x)
{
	for(int i = 0; i < NUMBER_OF_BLOCKS; i++)
	{
		xBlocks[i]++;
	}
	(*x)++;
}

void left(int xBlocks[], int *x)
{
	for(int i = 0; i < NUMBER_OF_BLOCKS; i++)
	{
		xBlocks[i]--;
	}
	(*x)--;
}

void down(int yBlocks[], int *y)
{
	for(int i = 0; i < NUMBER_OF_BLOCKS; i++)
	{
		yBlocks[i]++;
	}
	(*y)++;
}

void up(int yBlocks[], int *y)
{
	for(int i = 0; i < NUMBER_OF_BLOCKS; i++)
	{
		yBlocks[i]--;
	}
	(*y)--;
}

void shift(int yShift, int xShift, int *lastShiftX, int *lastShiftY, int xBlocks[], int yBlocks[], int *x, int *y)
{
	for(int i = 0; i < NUMBER_OF_BLOCKS; i++)
	{
		xBlocks[i] += xShift;
		yBlocks[i] += yShift;
	}
	*x += xShift;
	*y += yShift;
	*lastShiftX = xShift;
	*lastShiftY = yShift;
}

void rollbackShift(const int *lastShiftX, const int *lastShiftY, int xBlocks[], int yBlocks[], int *x, int *y)
{
	for(int i = 0; i < NUMBER_OF_BLOCKS; i++)
	{
		xBlocks[i] -= *lastShiftX;
		yBlocks[i] -= *lastShiftY;
	}
	*x -= *lastShiftX;
	*y -= *lastShiftY;
}

void rotateLeft(int shape, int *rotation, int xBlocks[], int yBlocks[], int x, int y)
{
	if(--*rotation < 0)
	{
		*rotation = 3;
	}
	updateShape(shape, *rotation, xBlocks, yBlocks, x, y);
}

void rotateRight(int shape, int *rotation, int xBlocks[], int yBlocks[], int x, int y)
{
	if(++*rotation > 3)
	{
		*rotation = 0;
	}
	updateShape(shape, *rotation, xBlocks, yBlocks, x, y);
}

void resetTetromino(uint64_t *timeSinceDown, int *rotation, int *shape, int *freeSlotsInBag, int bag[], int xBlocks[], int yBlocks[], int *x, int *y)
{
	if(*freeSlotsInBag == NUMBER_OF_SHAPES)
	{
		fillBag(bag, freeSlotsInBag);
	}
	*shape = bag[(*freeSlotsInBag)++];
	*x = DEFAULT_X;
	*y = DEFAULT_Y;
	*rotation = 0;
	updateShape(*shape, *rotation, xBlocks, yBlocks, *x, *y);
	*timeSinceDown = 0.0;
}

void cleanGrid(int grid[][HEIGHT])
{
	for(int i = 0; i < WIDTH; ++i)
	{
		for(int j = 0; j < HEIGHT; ++j)
		{
			grid[i][j] = -1;
		}
	}
}

int notValidPosition(const int xPos[], const int yPos[], int grid[][HEIGHT])
{
	for(int i = 0; i < NUMBER_OF_BLOCKS; i++)
	{
		if(xPos[i] < 0 || yPos[i] < 0 || xPos[i] >= WIDTH || yPos[i] >= HEIGHT || grid[xPos[i]][yPos[i]] != -1)
		{
			return TRUE;
		}
	}
	return FALSE;
}

void updateHint(int yHint[], int xBlocks[], const int yBlocks[], int grid[][HEIGHT])
{
	for(int i = 0; i < NUMBER_OF_BLOCKS; i++)
	{
		yHint[i] = yBlocks[i];
	}
	while(!notValidPosition(xBlocks, yHint, grid))
	{
		for(int i = 0; i < NUMBER_OF_BLOCKS; i++)
		{
			yHint[i]++;
		}
	}
	for(int i = 0; i < NUMBER_OF_BLOCKS; i++)
	{
		yHint[i]--;
	}
}

void toBackground(int shape, int grid[][HEIGHT], const int xBlocks[], const int yBlocks[])
{
	for(int i = 0; i < NUMBER_OF_BLOCKS; i++)
	{
		grid[xBlocks[i]][yBlocks[i]] = shape;
	}
}

int tryShift(int yShift, int xShift, int *lastShiftX, int *lastShiftY, int grid[][HEIGHT], int xBlocks[], int yBlocks[], int *x, int *y)
{
	shift(yShift, xShift, lastShiftX, lastShiftY, xBlocks, yBlocks, x, y);
	if(notValidPosition(xBlocks, yBlocks, grid))
	{
		rollbackShift(lastShiftX, lastShiftY, xBlocks, yBlocks, x, y);
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}

int kickRight(int rotation, int *lastShiftX, int *lastShiftY, int grid[][HEIGHT], int xBlocks[], int yBlocks[], int *x, int *y)
{
	switch(rotation)
	{
		case 0:
		{
			if(tryShift(-1, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-1, 1, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(0, -2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-1, -2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			break;
		}
		case 1:
		{
			if(tryShift(1, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(1, -1, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(0, 2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(1, 2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			break;
		}
		case 2:
		{
			if(tryShift(1, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(1, 1, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(0, -2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(1, -2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			break;
		}
		case 3:
		{
			if(tryShift(-1, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-1, -1, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(0, 2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-1, 2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			break;
		}
		default:
			printf("error");
	}
	return FALSE;
}

int kickLeft(int rotation, int *lastShiftX, int *lastShiftY, int grid[][HEIGHT], int xBlocks[], int yBlocks[], int *x, int *y)
{
	switch(rotation)
	{
		case 3:
		{
			if(tryShift(1, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(1, -1, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(0, 2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(1, 2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			break;
		}
		case 2:
		{
			if(tryShift(-1, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-1, 1, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(0, -2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-1, -2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			break;
		}
		case 1:
		{
			if(tryShift(-1, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-1, -1, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(0, 2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-1, 2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			break;
		}
		case 0:
		{
			if(tryShift(1, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(1, 1, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(0, -2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(1, -2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			break;
		}
		default:
			printf("error");
	}
	return FALSE;
}

int kickIRight(int rotation, int *lastShiftX, int *lastShiftY, int grid[][HEIGHT], int xBlocks[], int yBlocks[], int *x, int *y)
{
	switch(rotation)
	{
		case 0:
		{
			if(tryShift(-2, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(1, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-2, -1, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(1, 2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			break;
		}
		case 1:
		{
			if(tryShift(-1, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(2, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-1, 2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(2, -1, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			break;
		}
		case 2:
		{
			if(tryShift(2, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-1, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(2, 1, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-1, -2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			break;
		}
		case 3:
		{
			if(tryShift(1, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-2, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(1, -2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-2, 1, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			break;
		}
		default:
			printf("error");
	}
	return FALSE;
}

int kickILeft(int rotation, int *lastShiftX, int *lastShiftY, int grid[][HEIGHT], int xBlocks[], int yBlocks[], int *x, int *y)
{
	switch(rotation)
	{
		case 3:
		{
			if(tryShift(2, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-1, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(2, 1, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-1, -2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			break;
		}
		case 2:
		{
			if(tryShift(1, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-2, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(1, -2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-2, 1, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			break;
		}
		case 1:
		{
			if(tryShift(-2, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(1, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-2, -1, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(1, 2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			break;
		}
		case 0:
		{
			if(tryShift(-1, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(2, 0, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(-1, 2, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			if(tryShift(2, -1, lastShiftX, lastShiftY, grid, xBlocks, yBlocks, x, y))
			{
				return TRUE;
			}
			break;
		}
		default:
			printf("error");
	}
	return FALSE;
}

void restartGame()
{
	gameEnded[current_game] = FALSE;
	cleanGrid(grid1[current_game]);
	cleanGrid(grid2[current_game]);
	fillBag(bag1[current_game], &freeSlotsInBag1[current_game]);
	fillBag(bag2[current_game], &freeSlotsInBag2[current_game]);
	resetTetromino(&timeSinceDown1[current_game], &rotation1[current_game], &shape1[current_game], &freeSlotsInBag1[current_game], bag1[current_game], xBlocks1[current_game], yBlocks1[current_game], &xPos1[current_game], &yPos1[current_game]);
	resetTetromino(&timeSinceDown2[current_game], &rotation2[current_game], &shape2[current_game], &freeSlotsInBag2[current_game], bag2[current_game], xBlocks2[current_game], yBlocks2[current_game], &xPos2[current_game], &yPos2[current_game]);
}

void endGame()
{
	isGamePaused[current_game] = TRUE;
	gameEnded[current_game] = TRUE;
}

void moveDown(uint64_t *timeSinceDown, int *rotation, int *shape, int grid[][HEIGHT], int *freeSlotsInBag, int bag[], int xBlocks[], int yBlocks[], int *x, int *y)
{
	down(yBlocks, y);
	if(notValidPosition(xBlocks, yBlocks, grid))
	{
		up(yBlocks, y);
		toBackground(*shape, grid, xBlocks, yBlocks);
		resetTetromino(timeSinceDown, rotation, shape, freeSlotsInBag, bag, xBlocks, yBlocks, x, y);
		if(notValidPosition(xBlocks, yBlocks, grid))
		{
			endGame();
		}
	}
}

uint64_t get_time_in_milliseconds()
{
	clock_gettime(CLOCK_MONOTONIC_RAW, &timeNow);
	return timeNow.tv_sec * 1000 + timeNow.tv_nsec / 1000000;
}

int isAtBottom(int grid[][HEIGHT], int xBlocks[], int yBlocks[], int *y)
{
	down(yBlocks, y);
	int bottom = notValidPosition(xBlocks, yBlocks, grid);
	up(yBlocks, y);
	return bottom;
}

void updateGameState()
{
	if(playerRestart1[current_game] || playerRestart2[current_game])
	{
		playerRestart1[current_game] = FALSE;
		playerRestart2[current_game] = FALSE;
		isGamePaused[current_game] = FALSE;
		restartGame();
	}

	if (playerPause1[current_game] || playerPause2[current_game])
	{
		playerPause1[current_game] = FALSE;
		playerPause2[current_game] = FALSE;
		isGamePaused[current_game] = TRUE;
	}
	else if(playerUnpause1[current_game] || playerUnpause2[current_game])
	{
		playerUnpause1[current_game] = FALSE;
		playerUnpause2[current_game] = FALSE;
		isGamePaused[current_game] = FALSE;
	}
}

int update()
{
	if(removedRows1[current_game] > 2) //player 1 sending blocks to player 2
	{
		//printf("player 1 odstranil %d radku\n", removedRows1[current_game]);

		if(!gameEnded[current_game])
		{
			currentPlayer = 1;
		}

		for(int j = 0; j < 10; j += 2)
		{
			int xDrop[4];
			int yDrop[4];
			xDrop[0] = xDrop[2] = j;
			xDrop[1] = xDrop[3] = j + 1;
			yDrop[0] = yDrop[1] = 0;
			yDrop[2] = yDrop[3] = 1;

			while(!notValidPosition(xDrop, yDrop, grid2[current_game]))
			{
				for(int i = 0; i < 4; ++i)
				{
					yDrop[i]++;
				}
			}
			for(int i = 0; i < 4; ++i)
			{
				yDrop[i]--;
			}

			toBackground(1/*yellow color (square tetromino)*/, grid2[current_game], xDrop, yDrop);
		}
	}
	removedRows1[current_game] = 0;

	if(removedRows2[current_game] > 2) //player 2 sending blocks to player 1
	{
		//printf("player 2 odstranil %d radku\n", removedRows2[current_game]);

		if(!gameEnded[current_game])
		{
			currentPlayer = 0;
		}

		for(int j = 0; j < 10; j += 2)
		{
			int xDrop[4];
			int yDrop[4];
			xDrop[0] = xDrop[2] = j;
			xDrop[1] = xDrop[3] = j + 1;
			yDrop[0] = yDrop[1] = 0;
			yDrop[2] =  yDrop[3] = 1;

			while(!notValidPosition(xDrop, yDrop, grid1[current_game]))
			{
				for(int i = 0; i < 4; ++i)
				{
					yDrop[i]++;
				}
			}
			for(int i = 0; i < 4; ++i)
			{
				yDrop[i]--;
			}

			toBackground(1/*yellow color (square tetromino)*/, grid1[current_game], xDrop, yDrop);
		}
	}
	removedRows2[current_game] = 0;

	/*
	 * player 1
	 */

	if(!gameEnded[current_game])
	{
		currentPlayer = 0;
	}

	int didMove1 = FALSE;
	for(; playerD1[current_game] > 0; playerD1[current_game]--)
	{
		right(xBlocks1[current_game], &xPos1[current_game]);
		if(notValidPosition(xBlocks1[current_game], yBlocks1[current_game], grid1[current_game]))
		{
			left(xBlocks1[current_game], &xPos1[current_game]);
		}
		else
		{
			didMove1 = TRUE;
		}
	}
	for(; playerA1[current_game] > 0; playerA1[current_game]--)
	{
		left(xBlocks1[current_game], &xPos1[current_game]);
		if(notValidPosition(xBlocks1[current_game], yBlocks1[current_game], grid1[current_game]))
		{
			right(xBlocks1[current_game], &xPos1[current_game]);
		}
		else
		{
			didMove1 = TRUE;
		}
	}
	for(; playerQ1[current_game] > 0; playerQ1[current_game]--)
	{
		rotateLeft(shape1[current_game], &rotation1[current_game], xBlocks1[current_game], yBlocks1[current_game], xPos1[current_game], yPos1[current_game]);
		if(notValidPosition(xBlocks1[current_game], yBlocks1[current_game], grid1[current_game]))
		{
			if(shape1[current_game] == 0) //I
			{
				if(!(didMove1 = kickILeft(rotation1[current_game], &lastShiftX1[current_game], &lastShiftY1[current_game], grid1[current_game], xBlocks1[current_game], yBlocks1[current_game], &xPos1[current_game], &yPos1[current_game])))
				{
					rotateRight(shape1[current_game], &rotation1[current_game], xBlocks1[current_game], yBlocks1[current_game], xPos1[current_game], yPos1[current_game]);
				}
			}
			else
			{
				if(!(didMove1 = kickLeft(rotation1[current_game], &lastShiftX1[current_game], &lastShiftY1[current_game], grid1[current_game], xBlocks1[current_game], yBlocks1[current_game], &xPos1[current_game], &yPos1[current_game])))
				{
					rotateRight(shape1[current_game], &rotation1[current_game], xBlocks1[current_game], yBlocks1[current_game], xPos1[current_game], yPos1[current_game]);
				}
			}
		}
		else
		{
			didMove1 = TRUE;
		}
	}
	for(; playerE1[current_game] > 0; playerE1[current_game]--)
	{
		rotateRight(shape1[current_game], &rotation1[current_game], xBlocks1[current_game], yBlocks1[current_game], xPos1[current_game], yPos1[current_game]);
		if(notValidPosition(xBlocks1[current_game], yBlocks1[current_game], grid1[current_game]))
		{
			if(shape1[current_game] == 0) //I
			{
				if(!(didMove1 = kickIRight(rotation1[current_game], &lastShiftX1[current_game], &lastShiftY1[current_game], grid1[current_game], xBlocks1[current_game], yBlocks1[current_game], &xPos1[current_game], &yPos1[current_game])))
				{
					rotateLeft(shape1[current_game], &rotation1[current_game], xBlocks1[current_game], yBlocks1[current_game], xPos1[current_game], yPos1[current_game]);
				}
			}
			else
			{
				if(!(didMove1 = kickRight(rotation1[current_game], &lastShiftX1[current_game], &lastShiftY1[current_game], grid1[current_game], xBlocks1[current_game], yBlocks1[current_game], &xPos1[current_game], &yPos1[current_game])))
				{
					rotateLeft(shape1[current_game], &rotation1[current_game], xBlocks1[current_game], yBlocks1[current_game], xPos1[current_game], yPos1[current_game]);
				}
			}
		}
		else
		{
			didMove1 = TRUE;
		}
	}
	if(didMove1 && isAtBottom(grid1[current_game], xBlocks1[current_game], yBlocks1[current_game], &yPos1[current_game]))
	{
		timeSinceDown1[current_game] = get_time_in_milliseconds();
	}

	for(; playerS1[current_game] > 0; playerS1[current_game]--)
	{
		moveDown(&timeSinceDown1[current_game], &rotation1[current_game], &shape1[current_game], grid1[current_game], &freeSlotsInBag1[current_game], bag1[current_game], xBlocks1[current_game], yBlocks1[current_game], &xPos1[current_game], &yPos1[current_game]);
		timeSinceDown1[current_game] = get_time_in_milliseconds();
	}

	for(; playerSpace1[current_game] > 0; playerSpace1[current_game]--)
	{
		updateHint(yHint1[current_game], xBlocks1[current_game], yBlocks1[current_game], grid1[current_game]);
		for(int i = 0; i < NUMBER_OF_BLOCKS; i++)
		{
			yBlocks1[current_game][i] = yHint1[current_game][i];
		}
		toBackground(shape1[current_game], grid1[current_game], xBlocks1[current_game], yBlocks1[current_game]);
		resetTetromino(&timeSinceDown1[current_game], &rotation1[current_game], &shape1[current_game], &freeSlotsInBag1[current_game], bag1[current_game], xBlocks1[current_game], yBlocks1[current_game], &xPos1[current_game], &yPos1[current_game]);
	}

	if(timeSinceDown1[current_game] + 1000 <= get_time_in_milliseconds())
	{
		moveDown(&timeSinceDown1[current_game], &rotation1[current_game], &shape1[current_game], grid1[current_game], &freeSlotsInBag1[current_game], bag1[current_game], xBlocks1[current_game], yBlocks1[current_game], &xPos1[current_game], &yPos1[current_game]);
		timeSinceDown1[current_game] = get_time_in_milliseconds();
	}

	for(int i = 0; i < HEIGHT; i++)
	{
		for(int j = 0; j < WIDTH; j++)
		{
			if(grid1[current_game][j][i] == -1)
			{
				goto skipRow1;
			}
		}
		removedRows1[current_game]++;
		for(int j = 0; j < WIDTH; j++)
		{
			grid1[current_game][j][i] = -1;
		}
		for(int k = i; k > 0; k--)
		{
			for(int j = 0; j < WIDTH; j++)
			{
				grid1[current_game][j][k] = grid1[current_game][j][k - 1];
			}
		}
		skipRow1:;
	}

	updateHint(yHint1[current_game], xBlocks1[current_game], yBlocks1[current_game], grid1[current_game]);

	/*
	 * player 2
	 */

	if(!gameEnded[current_game])
	{
		currentPlayer = 1;
	}

	int didMove2 = FALSE;
	for(; playerD2[current_game] > 0; playerD2[current_game]--)
	{
		right(xBlocks2[current_game], &xPos2[current_game]);
		if(notValidPosition(xBlocks2[current_game], yBlocks2[current_game], grid2[current_game]))
		{
			left(xBlocks2[current_game], &xPos2[current_game]);
		}
		else
		{
			didMove2 = TRUE;
		}
	}
	for(; playerA2[current_game] > 0; playerA2[current_game]--)
	{
		left(xBlocks2[current_game], &xPos2[current_game]);
		if(notValidPosition(xBlocks2[current_game], yBlocks2[current_game], grid2[current_game]))
		{
			right(xBlocks2[current_game], &xPos2[current_game]);
		}
		else
		{
			didMove2 = TRUE;
		}
	}
	for(; playerQ2[current_game] > 0; playerQ2[current_game]--)
	{
		rotateLeft(shape2[current_game], &rotation2[current_game], xBlocks2[current_game], yBlocks2[current_game], xPos2[current_game], yPos2[current_game]);
		if(notValidPosition(xBlocks2[current_game], yBlocks2[current_game], grid2[current_game]))
		{
			if(shape2[current_game] == 0) //I
			{
				if(!(didMove2 = kickILeft(rotation2[current_game], &lastShiftX2[current_game], &lastShiftY2[current_game], grid2[current_game], xBlocks2[current_game], yBlocks2[current_game], &xPos2[current_game], &yPos2[current_game])))
				{
					rotateRight(shape2[current_game], &rotation2[current_game], xBlocks2[current_game], yBlocks2[current_game], xPos2[current_game], yPos2[current_game]);
				}
			}
			else
			{
				if(!(didMove2 = kickLeft(rotation2[current_game], &lastShiftX2[current_game], &lastShiftY2[current_game], grid2[current_game], xBlocks2[current_game], yBlocks2[current_game], &xPos2[current_game], &yPos2[current_game])))
				{
					rotateRight(shape2[current_game], &rotation2[current_game], xBlocks2[current_game], yBlocks2[current_game], xPos2[current_game], yPos2[current_game]);
				}
			}
		}
		else
		{
			didMove2 = TRUE;
		}
	}
	for(; playerE2[current_game] > 0; playerE2[current_game]--)
	{
		rotateRight(shape2[current_game], &rotation2[current_game], xBlocks2[current_game], yBlocks2[current_game], xPos2[current_game], yPos2[current_game]);
		if(notValidPosition(xBlocks2[current_game], yBlocks2[current_game], grid2[current_game]))
		{
			if(shape2[current_game] == 0) //I
			{
				if(!(didMove2 = kickIRight(rotation2[current_game], &lastShiftX2[current_game], &lastShiftY2[current_game], grid2[current_game], xBlocks2[current_game], yBlocks2[current_game], &xPos2[current_game], &yPos2[current_game])))
				{
					rotateLeft(shape2[current_game], &rotation2[current_game], xBlocks2[current_game], yBlocks2[current_game], xPos2[current_game], yPos2[current_game]);
				}
			}
			else
			{
				if(!(didMove2 = kickRight(rotation2[current_game], &lastShiftX2[current_game], &lastShiftY2[current_game], grid2[current_game], xBlocks2[current_game], yBlocks2[current_game], &xPos2[current_game], &yPos2[current_game])))
				{
					rotateLeft(shape2[current_game], &rotation2[current_game], xBlocks2[current_game], yBlocks2[current_game], xPos2[current_game], yPos2[current_game]);
				}
			}
		}
		else
		{
			didMove2 = TRUE;
		}
	}
	if(didMove2 && isAtBottom(grid2[current_game], xBlocks2[current_game], yBlocks2[current_game], &yPos2[current_game]))
	{
		timeSinceDown2[current_game] = get_time_in_milliseconds();
	}

	for(; playerS2[current_game] > 0; playerS2[current_game]--)
	{
		moveDown(&timeSinceDown2[current_game], &rotation2[current_game], &shape2[current_game], grid2[current_game], &freeSlotsInBag2[current_game], bag2[current_game], xBlocks2[current_game], yBlocks2[current_game], &xPos2[current_game], &yPos2[current_game]);
		timeSinceDown2[current_game] = get_time_in_milliseconds();
	}

	for(; playerSpace2[current_game] > 0; playerSpace2[current_game]--)
	{
		updateHint(yHint2[current_game], xBlocks2[current_game], yBlocks2[current_game], grid2[current_game]);
		for(int i = 0; i < NUMBER_OF_BLOCKS; i++)
		{
			yBlocks2[current_game][i] = yHint2[current_game][i];
		}
		toBackground(shape2[current_game], grid2[current_game], xBlocks2[current_game], yBlocks2[current_game]);
		resetTetromino(&timeSinceDown2[current_game], &rotation2[current_game], &shape2[current_game], &freeSlotsInBag2[current_game], bag2[current_game], xBlocks2[current_game], yBlocks2[current_game], &xPos2[current_game], &yPos2[current_game]);
	}

	if(timeSinceDown2[current_game] + 1000 <= get_time_in_milliseconds())
	{
		moveDown(&timeSinceDown2[current_game], &rotation2[current_game], &shape2[current_game], grid2[current_game], &freeSlotsInBag2[current_game], bag2[current_game], xBlocks2[current_game], yBlocks2[current_game], &xPos2[current_game], &yPos2[current_game]);
		timeSinceDown2[current_game] = get_time_in_milliseconds();
	}

	for(int i = 0; i < HEIGHT; i++)
	{
		for(int j = 0; j < WIDTH; j++)
		{
			if(grid2[current_game][j][i] == -1)
			{
				goto skipRow2;
			}
		}
		removedRows2[current_game]++;
		for(int j = 0; j < WIDTH; j++)
		{
			grid2[current_game][j][i] = -1;
		}
		for(int k = i; k > 0; k--)
		{
			for(int j = 0; j < WIDTH; j++)
			{
				grid2[current_game][j][k] = grid2[current_game][j][k - 1];
			}
		}
		skipRow2:;
	}
	updateHint(yHint2[current_game], xBlocks2[current_game], yBlocks2[current_game], grid2[current_game]);

	previousGameTime1[current_game] = previousGameTime2[current_game] = get_time_in_milliseconds();

	return gameEnded[current_game];
}

void resetInput()
{
	playerQ1[current_game] = FALSE;
	playerE1[current_game] = FALSE;
	playerA1[current_game] = FALSE;
	playerS1[current_game] = FALSE;
	playerD1[current_game] = FALSE;
	playerSpace1[current_game] = FALSE;
	playerUnpause1[current_game] = FALSE;
	playerPause1[current_game] = FALSE;
	playerRestart1[current_game] = FALSE;

	playerQ2[current_game] = FALSE;
	playerE2[current_game] = FALSE;
	playerA2[current_game] = FALSE;
	playerS2[current_game] = FALSE;
	playerD2[current_game] = FALSE;
	playerSpace2[current_game] = FALSE;
	playerUnpause2[current_game] = FALSE;
	playerPause2[current_game] = FALSE;
	playerRestart2[current_game] = FALSE;
}

void startGame()
{
	srand(time(NULL));

	for(current_game = 0; current_game < MAX_CLIENTS / 2; current_game++)
	{
		isGamePaused[current_game] = TRUE;
		resetInput();
		restartGame();
	}
}