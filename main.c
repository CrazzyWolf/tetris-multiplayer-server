#include <stdio.h>
#include <string.h> //strlen 
#include <stdlib.h>
#include <errno.h>
#include <unistd.h> //close 
#include <arpa/inet.h> //close
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros

#include "tetris.c"

#define PORT 10000
#define UPDATE_PERIOD 100
#define NICK_MAX_LENGTH 15

struct connection
{
	int client_socket;
	int game_index;
	char client_nick[NICK_MAX_LENGTH];
};

struct connection con[MAX_CLIENTS];

char game_position[MAX_CLIENTS][NICK_MAX_LENGTH];

int opt = TRUE;
int master_socket, addrlen, new_socket, activity, client, message_length, socket_descriptor, max_sd;
int client_errors[MAX_CLIENTS];
struct sockaddr_in address;
struct timeval tv;
uint64_t last_response[MAX_CLIENTS];
char read_buffer[1025]; //data read_buffer of 1K

//set of socket descriptors
fd_set readfds;

int get_other_player_game_index() //return game_index of other player
{
	return con[client].game_index + (con[client].game_index % 2 == 0 ? 1 : - 1);
}

int is_same_char_array(const char a1[], const char a2[], int length) //check if both char arrays are same
{
	for(int j = 0; j < length; ++j)
	{
		if(a1[j] != a2[j])
		{
			return FALSE;
		}
	}
	return TRUE;
}

int other_player_exists() //returns TRUE if other player is in game, FALSE otherwise
{
	int player_found = FALSE;
	int other_player_game_index = get_other_player_game_index();
	for(int i = 0; i < MAX_CLIENTS; ++i)
	{
		if(con[i].game_index == other_player_game_index)
		{
			player_found = TRUE;
			break;
		}
	}
	return player_found;
}

void sendChar(char c, int index) //send char to one client
{
	if(con[index].client_socket != 0)
	{
		char send_buffer[2];
		send_buffer[0] = c;
		send_buffer[1] = '\0';
		send(con[index].client_socket, send_buffer, 1, 0);
		printf("server to player %d: %s\n", index, send_buffer);
	}
}

void sendState(const char message[], int size) //fill buffer and send it to both clients
{
	int other_player_game_index = get_other_player_game_index();
	for(int i = 0; i < MAX_CLIENTS; ++i)
	{
		if(con[i].game_index == other_player_game_index)
		{
			char send_buffer[size + 1];
			for(int j = 0; j < size; ++j)
			{
				send_buffer[j] = message[j];
			}
			send_buffer[size] = '\0';
			send(con[client].client_socket, send_buffer, size, 0);
			printf("server to player %d: %s\n", client, send_buffer);
			send(con[i].client_socket, send_buffer, size, 0);
			printf("server to player %d: %s\n", i, send_buffer);
			break;
		}
	}
}


void disconnect_player() //disconnecting client and removing him from game and notify other player
{
	//Somebody disconnected, get his details and print
	getpeername(socket_descriptor, (struct sockaddr *) &address, \
                        (socklen_t *) &addrlen);
	printf("Host disconnected, ip %s, port %d \n",
	       inet_ntoa(address.sin_addr), ntohs(address.sin_port));

	//Close the socket and mark as 0 in list for reuse
	close(socket_descriptor);

	if(con[client].game_index != -1)
	{
		int other_player_game_index = get_other_player_game_index();
		int other_player_in_game = FALSE;
		int index = 0;
		for(; index < MAX_CLIENTS; ++index)
		{
			if(con[index].game_index == other_player_game_index)
			{
				other_player_in_game = TRUE;
				break;
			}
		}
		if(other_player_in_game == FALSE)
		{
			for(int i = 0; i < NICK_MAX_LENGTH; ++i)
			{
				game_position[con[client].game_index][i] = '_';
				game_position[other_player_game_index][i] = '_';
			}
		}
		else
		{
			sendChar('d', index);
		}

		con[client].game_index = -1;
	}
	for(int i = 0; i < NICK_MAX_LENGTH; ++i)
	{
		con[client].client_nick[i] = '_';
	}
	con[client].client_socket = 0;
}


void sendGrid(int game_index, char send_buffer[]) //sending buffer to both players
{
	for(int i = 0; i < MAX_CLIENTS; ++i)
	{
		if(is_same_char_array(con[i].client_nick, game_position[game_index], NICK_MAX_LENGTH))
		{
			send(con[i].client_socket, send_buffer, strlen(send_buffer), 0);
			//printf("server to player %d: %s\n", i, send_buffer);
			break;
		}
	}
}

void sendLobby() //send lobby to client
{
	int index = 0;
	int size = 1 /*lobby indicator*/
	           + MAX_CLIENTS * NICK_MAX_LENGTH /*name of every playing player*/;
	char send_buffer[size + 1];
	send_buffer[index++] = 'l';

	for(int i = 0; i < MAX_CLIENTS; ++i)
	{
		if(game_position[i][0] != '_')
		{
			for(int k = 0; k < NICK_MAX_LENGTH; ++k)
			{
				send_buffer[index++] = game_position[i][k];
			}
		}
		else
		{
			for(int j = 0; j < NICK_MAX_LENGTH; ++j)
			{
				send_buffer[index++] = '_';
			}
		}
	}

	send_buffer[size] = '\0';

	send(con[client].client_socket, send_buffer, size, 0);
	//printf("server to player %d: %s\n", client, send_buffer);
}

void sendUpdate(int game) //filling buffer with grid state and sending to both players
{
	int p1 = game * 2;
	int p2 = p1 + 1;
	if(game_position[p1][0] != '_' && game_position[p2][0] != '_')
	{
		int size = 1/*gird indicator*/
		           + WIDTH * HEIGHT/*grid*/
		           + NUMBER_OF_BLOCKS/*tetromino x*/
		           + NUMBER_OF_BLOCKS/*tetromino y*/
		           + NUMBER_OF_BLOCKS/*yHint*/
		           + 1/*shape*/
		           + WIDTH * HEIGHT/*grid*/
		           + NUMBER_OF_BLOCKS/*tetromino x*/
		           + NUMBER_OF_BLOCKS/*tetromino y*/
		           + NUMBER_OF_BLOCKS/*yHint*/
		           + 1/*shape*/;
		char send_buffer[size + 1];
		int bufferIndex = 0;

		send_buffer[bufferIndex++] = 'g';

		//player 1
		for(int width = 0; width < WIDTH; ++width)
		{
			for(int height = 0; height < HEIGHT; ++height)
			{
				send_buffer[bufferIndex++] = grid1[game][width][height] < 0 ? '_' : grid1[game][width][height] + '0';
			}
		}

		for(int j = 0; j < NUMBER_OF_BLOCKS; ++j)
		{
			send_buffer[bufferIndex++] = xBlocks1[game][j] + '0';
		}

		for(int j = 0; j < NUMBER_OF_BLOCKS; ++j)
		{
			send_buffer[bufferIndex++] = yBlocks1[game][j] + '0';
		}

		for(int j = 0; j < NUMBER_OF_BLOCKS; ++j)
		{
			send_buffer[bufferIndex++] = yHint1[game][j] + '0';
		}

		send_buffer[bufferIndex++] = shape1[game] + '0';

		for(int width = 0; width < WIDTH; ++width)
		{
			for(int height = 0; height < HEIGHT; ++height)
			{
				send_buffer[bufferIndex++] = grid2[game][width][height] < 0 ? '_' : grid2[game][width][height] + '0';
			}
		}

		for(int j = 0; j < NUMBER_OF_BLOCKS; ++j)
		{
			send_buffer[bufferIndex++] = xBlocks2[game][j] + '0';
		}

		for(int j = 0; j < NUMBER_OF_BLOCKS; ++j)
		{
			send_buffer[bufferIndex++] = yBlocks2[game][j] + '0';
		}

		for(int j = 0; j < NUMBER_OF_BLOCKS; ++j)
		{
			send_buffer[bufferIndex++] = yHint2[game][j] + '0';
		}

		send_buffer[bufferIndex] = shape2[game] + '0';

		send_buffer[size] = '\0';

		sendGrid(p1, send_buffer);
		sendGrid(p2, send_buffer);
	}
}


void handle_message()
{
	int game = con[client].game_index / 2;
	int player = con[client].game_index % 2; //-1 if player is not in game

	if(read_buffer[0] != 'p')
		printf("player %d: %s\n", client, read_buffer);

	for(int offset = 0; offset < message_length; offset++)
	{
		switch(read_buffer[offset]) //TODO odpojovat pri chybe
		{
			case 'd': //client wants to send tetromino one block to right
				if(player == 0)
				{
					playerD1[game]++;
				}
				else if(player == 1)
				{
					playerD2[game]++;
				}
				break;
			case 'a': //client wants to send tetromino one block to left
				if(player == 0)
				{
					playerA1[game]++;
				}
				else if(player == 1)
				{
					playerA2[game]++;
				}
				break;
			case 's': //client wants to send tetromino one block bellow
				if(player == 0)
				{
					playerS1[game]++;
				}
				else if(player == 1)
				{
					playerS2[game]++;
				}
				break;
			case 'q': //client wants to turn tetromino to left
				if(player == 0)
				{
					playerQ1[game]++;
				}
				else if(player == 1)
				{
					playerQ2[game]++;
				}
				break;
			case 'e': //client wants to turn tetromino to right
				if(player == 0)
				{
					playerE1[game]++;
				}
				else if(player == 1)
				{
					playerE2[game]++;
				}
				break;
			case 'x': //client wants to send tetromino to bottom
				if(player == 0)
				{
					playerSpace1[game]++;
				}
				else if(player == 1)
				{
					playerSpace2[game]++;
				}
				break;
			case 'c': //client wants to resume the game
				if(player == 0)
				{
					if(other_player_exists())
					{
						playerUnpause1[game] = TRUE;
						sendState("c", 1);
					}
					else
					{
						sendChar('n', client);
					}
				}
				else if(player == 1)
				{
					if(other_player_exists())
					{
						playerUnpause2[game] = TRUE;
						sendState("c", 1);
					}
					else
					{
						sendChar('n', client);
					}
				}
				break;
			case 'v': //client wants to pause the game
				if(player == 0)
				{
					playerPause1[game] = TRUE;
					sendState("v", 1);
				}
				else if(player == 1)
				{
					playerPause2[game] = TRUE;
					sendState("v", 1);
				}
				break;
			case 'y': //client wants to restart the game
				if(player == 0)
				{
					if(other_player_exists())
					{
						playerRestart1[game] = TRUE;
						sendState("c", 1);
					}
					else
					{
						sendChar('n', client);
					}
				}
				else if(player == 1)
				{
					if(other_player_exists())
					{
						playerRestart2[game] = TRUE;
					}
					else
					{
						sendChar('n', client);
					}
				}
				break;
			case 'l': //client wants lobby list
				sendLobby();
				break;
			case 'm':
			{
				if(1 + NICK_MAX_LENGTH <= message_length - offset)
				{
					int i = 0;
					for(; i < NICK_MAX_LENGTH; ++i)
					{
						con[client].client_nick[i] = read_buffer[offset + 1 + i];
					}
					for(; i < NICK_MAX_LENGTH; ++i)
					{
						con[client].client_nick[i] = '_';
					}

					offset += NICK_MAX_LENGTH;
					break;
				}
			}
			case 'g': //client wants to join lobby
			{
				int i = read_buffer[offset + 1] - '0';
				if(i <= 9 && i >= 0)
				{
					i *= 2;

					int can_enter_game = FALSE;
					if(game_position[i][0] != '_' && is_same_char_array(game_position[i], con[client].client_nick, NICK_MAX_LENGTH))
					{
						can_enter_game = TRUE; //player on first position is connecting player
					}
					else if(game_position[i + 1][0] != '_' && is_same_char_array(game_position[i + 1], con[client].client_nick, NICK_MAX_LENGTH))
					{
						can_enter_game = TRUE; //player on second position is connecting player
						i++;
					}
					else
					{
						if(game_position[i][0] != '_')
						{
							i++; //first slot is not empty
						}
						if(game_position[i][0] == '_')
						{
							can_enter_game = TRUE; //game has empty slot
						}
					}

					if(can_enter_game)
					{
						con[client].game_index = i;
						for(int k = 0; k < NICK_MAX_LENGTH; ++k)
						{
							game_position[i][k] = con[client].client_nick[k];
						}

						int other_player_game_index = get_other_player_game_index();
						for(int j = 0; j < MAX_CLIENTS; ++j)
						{
							if(con[j].game_index == other_player_game_index)
							{
								sendChar('o', j); //notify other player that opponent is connected
								sendChar('o', client);
								break;
							}
						}
					}

					offset++;
					break;
				}
			}
			case 'p': //ping
				break;
			default: //client sent invalid message
				client_errors[client]++;
				if(client_errors[client] >= 3)
				{
					disconnect_player();
				}
				offset = message_length;
				break;
		}
	}
}

int main(int argc, char *argv[])
{
	tv.tv_sec = 0;

	//a message 
	char *message = "w";

	//initialise all client_socket[] to 0 so not checked 
	for(client = 0; client < MAX_CLIENTS; client++)
	{
		con[client].client_socket = 0;
		con[client].game_index = -1;

		for(int i = 0; i < NICK_MAX_LENGTH; ++i)
		{
			con[client].client_nick[i] = '_';
			game_position[client][i] = '_';
		}
	}

	//create a master socket 
	if((master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0)
	{
		perror("socket failed");
		exit(EXIT_FAILURE);
	}

	//set master socket to allow multiple connections,
	//this is just a good habit, it will work without this 
	if(setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *) &opt,
	              sizeof(opt)) < 0)
	{
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	//type of socket created 
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr("10.147.19.206");//"127.0.0.1");//"10.10.110.23");
	address.sin_port = htons(PORT);

	//bind the socket to localhost port 8888 
	if(bind(master_socket, (struct sockaddr *) &address, sizeof(address)) < 0)
	{
		perror("bind failed");
		exit(EXIT_FAILURE);
	}
	printf("Listener on port %d \n", PORT);

	//try to specify maximum of 3 pending connections for the master socket 
	if(listen(master_socket, 3) < 0)
	{
		perror("listen");
		exit(EXIT_FAILURE);
	}

	//accept the incoming connection 
	addrlen = sizeof(address);
	puts("Waiting for connections...");

	startGame();
	uint64_t time_since_update = 0;

	while(TRUE)
	{
		//clear the socket set 
		FD_ZERO(&readfds);

		//add master socket to set 
		FD_SET(master_socket, &readfds);
		max_sd = master_socket;

		//add child sockets to set 
		for(client = 0; client < MAX_CLIENTS; client++)
		{
			//socket descriptor
			socket_descriptor = con[client].client_socket;

			//if valid socket descriptor then add to read list 
			if(socket_descriptor > 0)
			{
				FD_SET(socket_descriptor, &readfds);
			}

			//highest file descriptor number, need it for the select function 
			if(socket_descriptor > max_sd)
			{
				max_sd = socket_descriptor;
			}
		}

		tv.tv_usec = 1000;
		//wait for an activity on one of the sockets, timeout is NULL, so wait indefinitely
		activity = select(max_sd + 1, &readfds, NULL, NULL, &tv);

		if((activity < 0) && (errno != EINTR))
		{
			printf("select error");
		}

		//If something happened on the master socket, then its an incoming connection
		if(FD_ISSET(master_socket, &readfds))
		{
			if((new_socket = accept(master_socket,
			                        (struct sockaddr *) &address, (socklen_t *) &addrlen)) < 0)
			{
				perror("accept");
				exit(EXIT_FAILURE);
			}

			//inform user of socket number - used in send and receive commands 
			printf("New connection, socket fd is %d, ip is : %s, port : %d\n", new_socket,
			       inet_ntoa(address.sin_addr), ntohs
					       (address.sin_port));

			//send new connection greeting message 
			if(send(new_socket, message, strlen(message), 0) != strlen(message))
			{
				perror("send");
			}

			puts("Welcome message sent successfully");

			//add new socket to array of sockets 
			for(client = 0; client < MAX_CLIENTS; client++)
			{
				//if position is empty 
				if(con[client].client_socket == 0)
				{
					client_errors[client] = 0;
					con[client].client_socket = new_socket;
					printf("Adding to list of sockets as %d\n", client);
					break;
				}
			}
		}

		//else its some IO operation on some other socket 
		for(client = 0; client < MAX_CLIENTS; client++)
		{
			socket_descriptor = con[client].client_socket;
			if(FD_ISSET(socket_descriptor, &readfds))
			{
				//Check if it was for closing, and also read the incoming message
				if((message_length = read(socket_descriptor, read_buffer, 1024)) == 0)
				{
					if(last_response[client] + 1000 < get_time_in_milliseconds())
					{
						disconnect_player();
					}
				}
				else
				{
					handle_message();
					last_response[client] = get_time_in_milliseconds();

					for(int i = 0; i < message_length; ++i)
					{
						read_buffer[i] = '\0'; //remove all characters from buffer
					}
				}
			}
		}

		//update games after update period
		if(time_since_update + UPDATE_PERIOD < get_time_in_milliseconds())
		{
			time_since_update = get_time_in_milliseconds();

			for(current_game = 0; current_game < MAX_CLIENTS / 2; current_game++)
			{
				updateGameState();
				if(!isGamePaused[current_game])
				{
					if(update())
					{
						int player = current_game * 2 + (currentPlayer ? 0 : 1); //currentPlayer lost so other player is winner
						for(int j = 0; j < MAX_CLIENTS; ++j)
						{
							if(con[j].game_index == player)
							{
								client = j;
								break;
							}
						}

						char state[1 + NICK_MAX_LENGTH];
						state[0] = 'e';
						for(int i = 0; i < NICK_MAX_LENGTH; ++i)
						{
							state[i + 1] = con[client].client_nick[i];
						}
						sendState(state, 1 + NICK_MAX_LENGTH);
					}
					else
					{
						sendUpdate(current_game);
					}
				}
				else
				{
					uint64_t time = get_time_in_milliseconds();
					timeSinceDown1[current_game] += time - previousGameTime1[current_game];
					timeSinceDown2[current_game] += time - previousGameTime2[current_game];
					previousGameTime1[current_game] = previousGameTime2[current_game] = time;
				}
			}
			//printf("ted\n");
		}
	}
} 
