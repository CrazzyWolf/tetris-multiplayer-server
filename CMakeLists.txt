cmake_minimum_required(VERSION 3.10)
project(TetrisMP_Server C)

set(CMAKE_C_STANDARD 11)

add_executable(TetrisMP_Server main.c)